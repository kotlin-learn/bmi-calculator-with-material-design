package com.pakawat.bmicalculator

import android.app.Activity
import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.KeyEvent
import android.view.View
import android.view.inputmethod.InputMethodManager
import androidx.fragment.app.Fragment
import com.pakawat.bmicalculator.databinding.ActivityMainBinding
import kotlin.math.pow

class MainActivity : AppCompatActivity() {

    lateinit var binding : ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        binding.calculateButton.setOnClickListener { calculateBMI() }
        binding.weightEdt.setOnKeyListener { view, keyCode, _ -> handleKeyEvent(view, keyCode)}
        binding.heightEdt.setOnKeyListener { view, keyCode, _ -> handleKeyEvent(view, keyCode)}
    }


    private fun calculateBMI() {
        //รับ input
        val stringFromEdtWeight = binding.weightEdt.text.toString()
        val stringFromEdtHeight = binding.heightEdt.text.toString()
        //แปลง string -> double
        val weight = stringFromEdtWeight.toDoubleOrNull()
        var height = stringFromEdtHeight.toDoubleOrNull()

//        check ถ้า null หรือ 0.0
        if(weight==null || weight == 0.0||height==null || height==0.0){
            binding.result.visibility = View.INVISIBLE
            binding.heightEdt.setText("")
            binding.weightEdt.setText("")
            print("Null")
            return
        }

        //แปลง height จาก cm ไปเป็น m
        height/=100

        //หา bmi
        val bmi = weight / height.pow(2)
        binding.result.visibility = View.VISIBLE
        displayResult(bmi);
    }

    private fun displayResult(bmi: Double) {
        val formattedBmi = bmi.toString().substring(0,4)
        var bmiDes = if(bmi < 18.5){
            "Under Weight"
        }else if(bmi in 18.5 .. 22.9){
            "Normal"
        }else if(bmi in 23.0 .. 24.9){
            "Over Weight"
        }else if(bmi in 25.0 .. 29.9){
            "Obese"
        }else{
            "Extremely obese"
        }
        binding.result.text =getString(R.string.result ,formattedBmi,bmiDes  )

    }

    private fun handleKeyEvent(view: View, keyCode: Int): Boolean {
        if (keyCode == KeyEvent.KEYCODE_ENTER) {
            // Hide the keyboard
            val inputMethodManager =
                getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            inputMethodManager.hideSoftInputFromWindow(view.windowToken, 0)
            return true
        }
        return false
    }

}
